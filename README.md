## MSFoundation

### 介绍    
Foundation 基础库，包含常用宏定义和 Foundation 相关扩展

### 使用方法  
1.Podfile 添加下面代码，执行 pod install   

	 ms_pod "MSFoundation", :tag => "v1.0", :git => "git@gitlab.com:misslove1015/MSFoundation.git", :inhibit_warnings => false , :ms_debug => $msDebugSwitch, :ms_branch => $msBranch, :ms_useRemote => $msUseRemote
    
2.引入头文件
 
    #import <MSFoundation/MSFoundation.h>
   

