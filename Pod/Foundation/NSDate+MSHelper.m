//
//  NSDate+MSHelper.m
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/5.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

#import "NSDate+MSHelper.h"

@implementation NSDate (MSHelper)

// 日期转字符串
- (NSString *)ms_formatWithStyle:(NSString*)style {
    NSDateFormatter *dateFormatter = [[self class] dateFormatter];
    dateFormatter.dateFormat = style;
    return [dateFormatter stringFromDate:self];
}

// 字符串转日期
+ (NSDate *)ms_formatWithDateStr:(NSString *)dateStr style:(NSString *)style {
    NSDateFormatter *dateFormatter = [self dateFormatter];
    dateFormatter.dateFormat = style;
    return [dateFormatter dateFromString:dateStr];
}

// 时间戳转字符串
+ (NSString *)ms_formatWithTimestamp:(NSTimeInterval)timestamp style:(NSString *)style {
    NSDateFormatter *dateFormatter = [self dateFormatter];
    timestamp = timestamp / 1000.0;
    dateFormatter.dateFormat = style;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
    return [dateFormatter stringFromDate:date];
}

// 是否是今天
- (BOOL)ms_isToday {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:[NSDate date]];
    NSDate *today = [cal dateFromComponents:components];
    components = [cal components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:self];
    NSDate *otherDate = [cal dateFromComponents:components];
    return [today isEqualToDate:otherDate];
}

// 是否是明天
- (BOOL)ms_isTomorrow {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:[[NSDate date] dateByAddingDays:1]];
    NSDate *tomorrow = [cal dateFromComponents:components];
    components = [cal components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:self];
    NSDate *otherDate = [cal dateFromComponents:components];
    return [tomorrow isEqualToDate:otherDate];
}

// 是否是昨天
- (BOOL)ms_isYesterday {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:[[NSDate date] dateBySubtractingDays:1]];
    NSDate *tomorrow = [cal dateFromComponents:components];
    components = [cal components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:self];
    NSDate *otherDate = [cal dateFromComponents:components];
    return [tomorrow isEqualToDate:otherDate];
}

#pragma mark- private
+ (NSDateFormatter *)dateFormatter {
    static dispatch_once_t onceToken;
    static NSDateFormatter *dateFormatter = nil;
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc]init];
    });
    return dateFormatter;
}

+ (NSCalendar *)calendar {
    static dispatch_once_t onceToken;
    static NSCalendar *calendar = nil;
    dispatch_once(&onceToken, ^{
        calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    });
    return calendar;
}

- (NSDate *)dateByAddingDays:(NSInteger)days{
    NSCalendar *calendar = [[self class] calendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:days];
    return [calendar dateByAddingComponents:components toDate:self options:0];
}

- (NSDate *)dateBySubtractingDays:(NSInteger)days{
    NSCalendar *calendar = [[self class] calendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:-1*days];
    return [calendar dateByAddingComponents:components toDate:self options:0];
}

@end
