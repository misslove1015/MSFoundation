//
//  NSString+MSHelper.m
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/5.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

#import "NSString+MSHelper.h"
#import <CommonCrypto/CommonCrypto.h>

@implementation NSString (MSHelper)

// 去除首尾空格和换行
- (NSString *)ms_trim {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

// MD5 加密
- (NSString *)ms_MD5 {
    const char *cStr = [self UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr,(UInt32)strlen(cStr),digest);
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    return output;    
}

// 是否是纯数字
- (BOOL)ms_isNumber {
    NSString *text = [self stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
    if(text.length > 0) {
        return NO;
    }
    return YES;
}

// 是否是纯汉字
- (BOOL)ms_isChineseCharacters {
    BOOL result = NO;
    NSString *regex = @"[\u4e00-\u9fa5]+";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    result = [predicate evaluateWithObject:self];
    return result;
}

// 添加行高
- (NSAttributedString *)ms_addLineSpacing:(CGFloat)spacing {
    if (!self) return nil;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:spacing];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [self length])];
    return attributedString;
}

// 计算高度
- (CGFloat)ms_heightWithFontSize:(CGFloat)fontSize width:(CGFloat)width {
    CGRect rect = [self boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize]} context:nil];
    return rect.size.height;
}

@end
