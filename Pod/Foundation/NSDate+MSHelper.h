//
//  NSDate+MSHelper.h
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/5.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (MSHelper)

// 日期转字符串
- (NSString *)ms_formatWithStyle:(NSString*)style;

// 字符串转日期
+ (NSDate *)ms_formatWithDateStr:(NSString *)dateStr style:(NSString *)style;

// 时间戳转字符串
+ (NSString *)ms_formatWithTimestamp:(NSTimeInterval)timestamp style:(NSString *)style;

// 是否是今天
- (BOOL)ms_isToday;

// 是否是明天
- (BOOL)ms_isTomorrow;

// 是否是昨天
- (BOOL)ms_isYesterday;

@end
