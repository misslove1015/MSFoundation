//
//  NSString+MSHelper.h
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/5.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (MSHelper)

// 去除首尾空格和换行
- (NSString *)ms_trim;

// MD5 加密
- (NSString *)ms_MD5;

// 是否是纯数字
- (BOOL)ms_isNumber;

// 是否是纯汉字
- (BOOL)ms_isChineseCharacters;

// 添加行高
- (NSAttributedString *)ms_addLineSpacing:(CGFloat)spacing;

// 计算高度
- (CGFloat)ms_heightWithFontSize:(CGFloat)fontSize width:(CGFloat)width;

@end
