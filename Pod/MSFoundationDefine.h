//
//  MSFoundationDefine.h
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/5.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

// 常用宏定义

#ifndef MSFoundationDefine_h
#define MSFoundationDefine_h

#define SCREEN_WIDTH UIScreen.mainScreen.bounds.size.width
#define SCREEN_HEIGHT UIScreen.mainScreen.bounds.size.height
#define IPHONEX (SCREEN_HEIGHT == 812)
#define NAV_HEIGHT (IPHONEX?88:64)
#define TABBAR_HEIGHT (IPHONEX?83:49)
#define STATUSBAR_HEIGHT (IPHONEX?44:20)
#define SAFE_BOTTOM_HEIGHT (IPHONEX?34:0)

#define COLOR_HEX(hexValue) [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16)) / 255.0 green:((float)((hexValue & 0xFF00) >> 8)) / 255.0 blue:((float)(hexValue & 0xFF)) / 255.0 alpha:1.0f]
#define COLOR_HEXA (hexValue,A) [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16)) / 255.0 green:((float)((hexValue & 0xFF00) >> 8)) / 255.0 blue:((float)(hexValue & 0xFF)) / 255.0 alpha:A]

#define COLOR(R, G, B) [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:1]
#define COLORA(R, G, B, A) [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:A]
#define COLOR_RANDOM [UIColor colorWithRed:arc4random()%256/255.0 green:arc4random()%256/255.0 blue:arc4random()%256/255.0 alpha:1]


#define FONT(SIZE) [UIFont systemFontOfSize:SIZE]
#define FONTB(SIZE) [UIFont boldSystemFontOfSize:SIZE]

#define STRING_FROM_CLASSS(CLASS) NSStringFromClass([CLASS class])
#define STRING_FROM_INT(INT) [NSString stringWithFormat:@"%ld", INT]
#define SAFE_STRING(STRING) STRING?STRING:@""
#define NIB(CLASS) [UINib nibWithNibName:STRING_FROM_CLASSS(CLASS) bundle:nil]

#define WEAK(obj) __weak typeof(obj) obj##Weak = obj;
#define STRONG(obj) __strong typeof(obj) obj = obj##Weak;

#define USER_DEFAULTS [NSUserDefaults standardUserDefaults]
#define FILE_MANAGER [NSFileManager defaultManager]
#define NOTI_CENTER [NSNotificationCenter defaultCenter]
#define APPLICATION [UIApplication sharedApplication]

#endif /* MSFoundationDefine_h */
